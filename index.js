const net = require('net');
var config = {
    ip : null,
    port: null
}
var client;


exports.Configure = function(ip = "localhost", port="4273"){
    config.ip = ip;
    config.port = port;
}

exports.Connect = function (){
    client = new net.Socket();

    client.connect(config.port, config.ip, () =>{
        console.log('Connected succesfully');
    })
}

exports.Close = function(){
    client.end();
}


exports.Send = function(object, json = true){
    var buffer = CreateBuffer(object, json);
    if(!buffer){
        return;
    }

    client.write(buffer);
}
exports.Listen = function(callbackFunction){
    client.on('data', callbackFunction)

}

exports.ConvertBinaryToObject = function(data){
    var data = data.slice(4)

    var binary = new TextDecoder("utf-16le").decode(data);

    return JSON.parse(binary);
}

function CreateBuffer(object, json){
    if(!json){
        return false;
    }
    else{
        var json = JSON.stringify(object);
        var string_size = 4 + (Buffer.byteLength(json) * 2);
        console.log(string_size);
        var hex_of_size = string_size.toString(16);

        if(hex_of_size.length < 8){
            var remaining_zeros = 8 - hex_of_size.length;
            for(remaining_zeros; remaining_zeros > 0; remaining_zeros--){
                hex_of_size = "0" + hex_of_size;
            }
        }

        var i = 0;
        var array_per_byte = hex_of_size.match(/.{1,2}/g)
        var new_string = ""
        var length = array_per_byte.length;
        while(length > 0){
            length--;
            new_string += array_per_byte[length];
        }

        hex_of_size = "0x" + new_string;

        console.log(hex_of_size);
        //var buffer2 = new Buffer();
        //buffer2.write(json, 0, "utf-16le");
        //16402
        //Buffer.byteLength(buffer2);
        var buffer = new Buffer(string_size)
        buffer.writeUInt32BE(hex_of_size, 0);
        buffer.write(json, 4, "utf-16le");
        console.log(buffer);
        console.log(Buffer.byteLength(buffer));


        return buffer;
    }
}
