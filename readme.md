# How to use this plugin
## Installation
```
npm add git+https://git.dmssolutions.eu/delcom/plugin-dms-communication.git
```


## Add the plugin to your project
Require the plugin into your project
```
var dms = require('plugin-dms-communication')
```

## Configuration
**Call the Configure function:**

The configure function has 2  ***OPTIONAL*** parameters, address and port.

Example:   
```
dms.Configure("localhost","4273"); 
```

## Starting your connection
**Call the connect function**

Make sure you call the Configure function first.

Example: 
```
dms.Connect();
```
## Listen for messages
**Call the listen function**

The listen function expects a callback so that when it gets a message it knows what function to call 

Example:
```
var callbackFunction = function(data){
    //do something with your data
}

dms.Listen(callbackFunction);
```

## Send a message
**Call the send function**

The send function expects an object and a json parameter. The json parameter by default = true and for now doesnt do anything.

Example:
```
    dms.Send({
        Tasks : [{
            Task_name : "Print_card",
            Parameters : {
                Person_id: this.user_id,
                Printer_name: "Evolis Zenius",
                Printer_auto_flip: false,
                Template_front: "Delcom front",
                Template_back: ""
            }
        }]
    });
```
